﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndingSequence : MonoBehaviour
{
    [SerializeField] private int endingThreshold = default;

    private void OnTriggerEnter(Collider other)
    {
        Debug.Log(other);

        if (other == other.GetComponent<CharacterController>())
        {
            CalculateGameEnding();
        }
    }

    private void CalculateGameEnding()
    {
        if (LogicManager.Instance.PlayerEntityInstance.Humanity < endingThreshold)
        {
            BadEnding();
        }
        else
        {
            GoodEnding();
        }    
    }

    private void BadEnding()
    {
        UIManager.Instance.EndingText.text = "Jest już za późno. Twoja dusza scaliła się z krainą umarłych, straciłaś swoje człowieczeństwo. Nie ma powrotu.";
        ShowEndingUI();
    }

    private void GoodEnding()
    {
        UIManager.Instance.EndingText.text = "Widzisz osobę, którą poszukiwałaś! Jeszcze jest nadzieja. Możecie razem wrócić do świata żywych, zostawiając przeszłość za sobą.";
        ShowEndingUI();
    }

    private void ShowEndingUI()
    {
        Time.timeScale = 1 - Time.timeScale;
        Cursor.visible = !Cursor.visible;
        UIManager.Instance.EndingUI.SetActive(true);
    }
}
