﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingPlatform : MonoBehaviour
{
    public GameObject Player;

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject == Player)
        {
           Player.transform.parent = transform;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject == Player)
        {
            Player.transform.parent = null;
        }
    }

    //public GameObject Player;

    /*private PlayerEntity playerEntity;


    private void OnTriggerEnter(Collider other)
    {
        playerEntity = LogicManager.Instance.PlayerEntityInstance;

        if (other == GetComponent<PlayerEntity>())
        {
            playerEntity.gameObject.transform.parent = transform;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        //if (other.gameObject == Player)
        //{
        //    Player.transform.parent = null;
        //}
        if (other == GetComponent<PlayerEntity>())
        {
            playerEntity.gameObject.transform.parent = null;
        }
    }   */
}
