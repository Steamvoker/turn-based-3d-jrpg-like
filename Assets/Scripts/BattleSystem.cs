﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Cinemachine;
using UnityEngine.SceneManagement;

public enum BattleState { START, PLAYER_TURN, ENEMY_TURN, WIN, LOSS }

public class BattleSystem : MonoBehaviour
{
    #region Declarations
    //[SerializeField] private Transform playerBattlePosition = null;
    //[SerializeField] private Transform enemyBattlePosition = null;

    private PlayerEntity playerEntity = null;
    private EnemyEntity enemyEntity = null;
    private LivingEntity attacker = null;
    private LivingEntity defender = null;
    private BattleState battleState = default;
    private PlayerStats playerStats = null;
    private BattleStats enemyStats = null;
    private TMP_Text combatNarrationText = null;
    private CinemachineVirtualCamera battleCamera = null;
    private PlayerController playerController = null;
    private MouseLook mouseLook = null;

    public LivingEntity Attacker { get => attacker; set => attacker = value; }
    public LivingEntity Defender { get => defender; set => defender = value; }
    public BattleState BattleState { get => battleState; set => battleState = value; }
    public PlayerStats PlayerStats { get => playerStats; set => playerStats = value; }
    public BattleStats EnemyStats { get => enemyStats; set => enemyStats = value; }
    public EnemyEntity EnemyEntity { get => enemyEntity; set => enemyEntity = value; }
    public PlayerEntity PlayerEntity { get => playerEntity; set => playerEntity = value; }
    #endregion Declarations

    private void Start()
    {
        BattleState = BattleState.START;

        PlayerEntity = LogicManager.Instance.PlayerEntityInstance;
        PlayerEntity.ToggleCreateAbilityButtons();
        PlayerEntity.InitializeAbilityButtons();
        playerController = PlayerEntity.gameObject.GetComponent<PlayerController>();
        playerController.enabled = false;
        mouseLook = PlayerEntity.GetComponent<MouseLook>();
        mouseLook.enabled = false;
        PlayerStats = UIManager.Instance.PlayerStats;
        UIManager.Instance.AbilityButtonsParent.gameObject.SetActive(!UIManager.Instance.AbilityButtonsParent.gameObject.activeSelf);

        EnemyEntity = LogicManager.Instance.CurrentEnemy;
        EnemyStats = UIManager.Instance.EnemyStats;

        combatNarrationText = UIManager.Instance.CombatNarrationText;

        battleCamera = EnemyEntity.BattleSceneCamera;
        
        Cursor.visible = !Cursor.visible;

        StartCoroutine(SetupBattle());
    }

    private IEnumerator SetupBattle()
    {
        PlayerEntity.gameObject.transform.position = EnemyEntity.PlayerBattlePosition.position;
        PlayerEntity.gameObject.transform.rotation = EnemyEntity.PlayerBattlePosition.rotation;
        PlayerStats.gameObject.SetActive(!PlayerStats.gameObject.activeSelf);
        PlayerStats.HUDSetup(PlayerEntity);
        //playerController.Animator.SetLayerWeight(1, 1);
        playerController.Animator.SetBool("Battle", true);

        EnemyEntity.gameObject.transform.position = EnemyEntity.EnemyBattlePosition.position;
        EnemyEntity.gameObject.transform.rotation = EnemyEntity.EnemyBattlePosition.rotation;
        EnemyStats.gameObject.SetActive(!EnemyStats.gameObject.activeSelf);
        EnemyStats.HUDSetup(EnemyEntity);

        combatNarrationText.gameObject.SetActive(!combatNarrationText.gameObject.activeSelf);
        combatNarrationText.text = $"Oh? You're approaching me?";

        battleCamera.gameObject.SetActive(!battleCamera.gameObject.activeSelf);

        yield return new WaitForSeconds(2f);

        BattleState = BattleState.PLAYER_TURN;
        ReBindEntities();
        PlayerTurn();
    }

    public void PlayerTurn()
    {
        ToggleButtons();
        combatNarrationText.text = $"Choose an action.";
    }

    public IEnumerator EnemyTurn()
    {
        ToggleButtons();

        combatNarrationText.text = Attacker.LivingEntityName + " attacks.";

        yield return new WaitForSeconds(2f);

        EnemyEntity.UseRandomAbility();
    }

    public IEnumerator EndBattle()
    {
        if (BattleState == BattleState.WIN)
        {
            combatNarrationText.text = "You won!";
            ToggleButtons();

            yield return new WaitForSeconds(3f);

            EnemyEntity.gameObject.SetActive(false);

            PlayerStats.gameObject.SetActive(!PlayerStats.gameObject.activeSelf);
            EnemyStats.gameObject.SetActive(!EnemyStats.gameObject.activeSelf);
            combatNarrationText.gameObject.SetActive(!combatNarrationText.gameObject.activeSelf);
            UIManager.Instance.AbilityButtonsParent.gameObject.SetActive(!UIManager.Instance.AbilityButtonsParent.gameObject.activeSelf);
            PlayerEntity.ToggleCreateAbilityButtons();
            PlayerEntity.DeinitializeAbilityButtons();
            playerController.enabled = true;
            //playerController.Animator.SetLayerWeight(1, 0);
            playerController.Animator.SetBool("Battle", false);
            mouseLook.enabled = true;
            Cursor.visible = !Cursor.visible;
            BattleSceneManager.Instance.InvokeSceneUnload();
        }
        else if (BattleState == BattleState.LOSS)
        {
            combatNarrationText.text = "You lost.";

            yield return new WaitForSeconds(3f);

            StartupSceneManager.LoadSceneAsyncAdditive("MainMenu");
            SceneManager.UnloadSceneAsync("BattleScene");
            SceneManager.UnloadSceneAsync("FreeRoamScene");
            SceneManager.UnloadSceneAsync("Logic");
            SceneManager.UnloadSceneAsync("UI");
        }
    }

    public void ReBindEntities()
    {
        if (BattleState == BattleState.PLAYER_TURN)
        {
            Attacker = PlayerEntity;
            Defender = EnemyEntity;
        }
        else if (BattleState == BattleState.ENEMY_TURN)
        {
            Attacker = EnemyEntity;
            Defender = PlayerEntity;
        }
    }

    private void ToggleButtons()
    {
        for (int i = 0; i < UIManager.Instance.Buttons.Length; i++)
        {
            Button button = UIManager.Instance.Buttons[i];
            button.interactable = !button.interactable;
        }
    }
}
