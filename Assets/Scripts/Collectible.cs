﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
public class Collectible : MonoBehaviour
{
    #region Declarations
    [SerializeField] private TMP_Text collectibleText = null;

    private GameObject _collectibleTextUI = null;
    private PlayerController playerController = null;
    private MouseLook mouseLook = null;
    private Animation_control_script animator = null;
    #endregion Declarations

    private void Start()
    {
        _collectibleTextUI = UIManager.Instance.CollectibleTextUI;
        playerController = LogicManager.Instance.PlayerController;
        mouseLook = LogicManager.Instance.MouseLook;
        animator = LogicManager.Instance.PlayerAnimator;
    }

    private void OnTriggerStay(Collider other)
    {
        if (Input.GetKeyDown(KeyCode.E) && other.GetComponent<PlayerEntity>())
            ToggleCollectibleText();
    }

    private void ToggleCollectibleText()
    {
        var _tmpText = _collectibleTextUI.GetComponentInChildren<TMP_Text>();
        _tmpText.text = collectibleText.text;
        _collectibleTextUI.SetActive(!_collectibleTextUI.gameObject.activeSelf);
        playerController.enabled = !playerController.enabled;
        mouseLook.enabled = !mouseLook.enabled;
        animator.enabled = !animator.enabled;
    }
}
