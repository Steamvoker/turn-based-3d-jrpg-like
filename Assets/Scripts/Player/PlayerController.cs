﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    #region Declarations
    [SerializeField] private float movementSpeed = default;
    [SerializeField] private int maxJumpCount = default;
    [SerializeField] private float jumpForce = default;
    [SerializeField] private CharacterController characterController = null;
    [SerializeField] private Transform followTransform = null;
    [SerializeField] private Animator animator = null;

    private const float GRAV_ACCELERATION = -5f; //-9.81f

    private Vector3 directionalForce = default;
    private Vector3 cameraDirection = default;
    private float xAxis = default;
    private float zAxis = default;
    private int currentJumpCount = default;
    private bool jumpInput = default;

    public Animator Animator { get => animator; set => animator = value; }
    public float XAxis { get => xAxis; set => xAxis = value; }

    #endregion Declarations

    private void Awake()
    {
        currentJumpCount = maxJumpCount;
    }


    private void Update()
    {
        cameraDirection = followTransform.forward;

        XAxis = Input.GetAxis("Horizontal");
        zAxis = Input.GetAxis("Vertical");

        if (Input.GetKeyDown(KeyCode.Space))
            jumpInput = true;

        //animator.SetBool("isMoving", true);

        
    }

    private void FixedUpdate()
    {
        Move();
        if (jumpInput)
        {
            Jump();
            jumpInput = !jumpInput;
        }

        //if (Input.GetKey(KeyCode.W))
        //{
        //    animator.SetBool("isMoving", true);
        //}

        if (gameObject.transform.position.y < -20)
        {
            characterController.transform.position = new Vector3(0, 2.5f, 1);
        }
    }

    private void Move()
    {
        if (characterController.isGrounded && directionalForce.y < 0)
            directionalForce.y = -2f;

        directionalForce.y += GRAV_ACCELERATION * Time.deltaTime;

        Vector3 _move = transform.right * XAxis + directionalForce + cameraDirection * zAxis;
        characterController.Move(_move * movementSpeed * Time.deltaTime);
        //animator.SetBool("isMoving", true);

        //if (zAxis != 0 || XAxis != 0)
        //{
        //    Animator.SetBool("isMoving", true);
        //}
        //else
        //{
        //    Animator.SetBool("isMoving", false);
        //}
    }

    private void Jump()
    {
        if (characterController.isGrounded)
            currentJumpCount = maxJumpCount;

        if (currentJumpCount <= 0)
            return;
        else
        {
            directionalForce.y += jumpForce * Time.deltaTime;
            currentJumpCount--;
        }
    }
}
