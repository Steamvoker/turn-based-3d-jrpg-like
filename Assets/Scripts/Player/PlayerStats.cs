﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class PlayerStats : BattleStats
{
    #region Declarations
    [SerializeField] private TMP_Text humanityText = null;
    #endregion

    public override void HUDSetup(LivingEntity livingEntity)
    {
        base.HUDSetup(livingEntity);
        SetHumanityText();
    }

    public void SetHumanityText()
    {
        humanityText.text = "Humanity: " + LogicManager.Instance.PlayerEntityInstance.Humanity;
    }
}
