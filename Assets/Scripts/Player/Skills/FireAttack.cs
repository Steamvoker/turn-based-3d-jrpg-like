﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireAttack : Ability
{
    #region Declarations
    [SerializeField] private int humanityDamage = default;

    private PlayerController playerController = null;
    #endregion
    GameObject audioManager;
    public Animator EnemyAnimator;

    private void Start()
    {
        playerController = LogicManager.Instance.PlayerEntityInstance.gameObject.GetComponent<PlayerController>();
    }

    public override void PerformAbility()
    {
        if (battleSystem.Attacker == LogicManager.Instance.PlayerEntityInstance)
        {
            if (LogicManager.Instance.PlayerEntityInstance.Humanity <= 0)
            {
                LogicManager.Instance.PlayerEntityInstance.CurrentHealth -= humanityDamage;
            }
            else
            {
                LogicManager.Instance.PlayerEntityInstance.Humanity -= humanityDamage;
            }
            playerController.Animator.SetTrigger("Attack");
            EnemyAnimator.SetTrigger("Damage");
        }

        // CUSTOM EFFECTS GO HERE
        FindObjectOfType<AudioManager>().Play("FireHit");

        base.PerformAbility();
    }
}
