﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RestoreHumanity : Ability
{
    [SerializeField] private int humanityAmount = default;

    public override void PerformAbility()
    {
        PlayerEntity _player = gameObject.GetComponent<PlayerEntity>();

        if (_player != null)
        {
            _player.Humanity += RandomDamage(damage);
        }

        base.PerformAbility();
    }
}
