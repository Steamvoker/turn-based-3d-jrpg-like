﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public abstract class Ability : MonoBehaviour
{
    #region Declarations
    [SerializeField] private string abilityName;
    [SerializeField] private ElementalWeakness elementType;
    [SerializeField] protected int damage = default;

    protected BattleSystem battleSystem = null;
    private bool isDead = false;
    private bool performed = false;
    private int randomDamage;

    public string AbilityName { get => abilityName; set => abilityName = value; }
    public bool Performed { get => performed; set => performed = value; }
    #endregion Declarations

    private void Update()
    {
        if (SceneManager.GetSceneByName("BattleScene").isLoaded)
        {
            battleSystem = BattleSceneManager.Instance.BattleSystem;
        }
    }

    public virtual void PerformAbility()
    {
        StartCoroutine(TakeDamage());
        WinCheck();
    }

    protected void WinCheck()
    {
        if (isDead && battleSystem.Defender == battleSystem.EnemyEntity)
        {
            battleSystem.BattleState = BattleState.WIN;
            battleSystem.EnemyStats.SetHP(battleSystem.Defender.CurrentHealth);
            StartCoroutine(battleSystem.EndBattle());
        }
        else if (isDead && battleSystem.Defender == battleSystem.PlayerEntity)
        {
            battleSystem.BattleState = BattleState.LOSS;
            StartCoroutine(battleSystem.EndBattle());
        }
        else if (!isDead && battleSystem.Defender == battleSystem.EnemyEntity)
        {
            battleSystem.BattleState = BattleState.ENEMY_TURN;
            battleSystem.EnemyStats.SetHP(battleSystem.Defender.CurrentHealth);
            battleSystem.ReBindEntities();
            StartCoroutine(battleSystem.EnemyTurn());
        }
        else if (!isDead && battleSystem.Defender == battleSystem.PlayerEntity)
        {
            battleSystem.BattleState = BattleState.PLAYER_TURN;
            battleSystem.PlayerStats.SetHP(battleSystem.Defender.CurrentHealth);
            battleSystem.PlayerStats.SetHumanityText();
            battleSystem.ReBindEntities();
            battleSystem.PlayerTurn();
        }
    }

    private IEnumerator TakeDamage()
    {
        isDead = battleSystem.Defender.TakeDamage(RandomDamage(damage), elementType);

        yield return new WaitForSeconds(2f);
    }

    protected int RandomDamage(int damage)
    {
        return randomDamage = UnityEngine.Random.Range((int)(damage * 0.75f), (int)(damage * 1.25f));
    }
}
