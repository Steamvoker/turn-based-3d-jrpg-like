﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PhysicalAttack : Ability
{
    #region Declarations
    private PlayerController playerController = null;
    #endregion
    public Animator EnemyAnimator;

    private void Start()
    {
        playerController = LogicManager.Instance.PlayerEntityInstance.gameObject.GetComponent<PlayerController>();
    }

    public override void PerformAbility()
    {
        if (battleSystem.Attacker == LogicManager.Instance.PlayerEntityInstance)
        {
            playerController.Animator.SetTrigger("Attack");
            EnemyAnimator.SetTrigger("Damage");
        }

        // CUSTOM EFFECTS GO HERE
        //FindObjectOfType<AudioManager>().Play("BasicHit");

        base.PerformAbility();
    }
}

