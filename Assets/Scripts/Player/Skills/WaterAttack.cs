﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaterAttack : Ability
{
    #region Declarations
    [SerializeField] private int humanityDamage = default;

    private PlayerController playerController = null;
    #endregion
    public Animator EnemyAnimator;

    private void Start()
    {
        playerController = LogicManager.Instance.PlayerEntityInstance.gameObject.GetComponent<PlayerController>();
    }

    public override void PerformAbility()
    {
        if (battleSystem.Attacker == LogicManager.Instance.PlayerEntityInstance)
        {
            LogicManager.Instance.PlayerEntityInstance.Humanity -= humanityDamage;
            playerController.Animator.SetTrigger("Attack");
            EnemyAnimator.SetTrigger("Damage");
        }

        // CUSTOM EFFECTS GO HERE
        FindObjectOfType<AudioManager>().Play("WaterHit");

        base.PerformAbility();
    }
}
