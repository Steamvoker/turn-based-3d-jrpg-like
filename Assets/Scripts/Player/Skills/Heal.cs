﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Heal : Ability
{
    [SerializeField] private int healAmount = default;

    public override void PerformAbility()
    {
        LivingEntity _thisEntity = gameObject.GetComponent<LivingEntity>();

        if (_thisEntity != null)
        {
            _thisEntity.CurrentHealth += RandomDamage(healAmount);
            battleSystem.PlayerStats.SetHP(_thisEntity.CurrentHealth);
        }

        base.PerformAbility();
    }
}
