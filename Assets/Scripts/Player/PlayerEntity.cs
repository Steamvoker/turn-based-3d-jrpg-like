﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class PlayerEntity : LivingEntity
{
    #region Declarations
    [SerializeField] private int humanity = default;
    [SerializeField] private Ability[] abilityArray = null;

    public int Humanity { get => humanity; set => humanity = Mathf.Clamp(value, 0, 100); }

    #endregion Declarations

    public void InitializeAbilityButtons()
    {
        for (int i = 0; i < UIManager.Instance.Buttons.Length; i++)
        {
            Button button = UIManager.Instance.Buttons[i];

            int buttonIndex = i;
            button.onClick.AddListener(() => abilityArray[buttonIndex].PerformAbility());
        }
    }

    public void DeinitializeAbilityButtons()
    {
        for (int i = 0; i < UIManager.Instance.Buttons.Length; i++)
        {
            Button button = UIManager.Instance.Buttons[i];

            int buttonIndex = i;
            button.onClick.RemoveAllListeners();
        }
    }

    public void ToggleCreateAbilityButtons()
    {
        for (int i = 0; i < UIManager.Instance.Buttons.Length; i++)
        {
            if (i >= abilityArray.Length)
            {
                UIManager.Instance.Buttons[i].gameObject.SetActive(false);
                continue;
            }

            string buttonText = abilityArray[i].AbilityName;

            UIManager.Instance.Buttons[i].gameObject.SetActive(!UIManager.Instance.Buttons[i].gameObject.activeSelf);
            UIManager.Instance.Buttons[i].GetComponentInChildren<TMP_Text>().text = buttonText;
        }
    }
}
