﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseLook : MonoBehaviour
{
    #region Declarations
    [SerializeField] private CharacterController characterController = null;
    [SerializeField] private float cameraSensitivity = 100f;
    [SerializeField] private Transform playerTransform = null;
    [SerializeField] private Transform cameraFollowTarget = null;

    private float xRotation = 0f;
    private float yRotation = 0f;
    private float mouseX = default;
    #endregion Declarations

    //private void Start()
    //{
    //    Cursor.lockState = CursorLockMode.Locked;
    //}

    private void Update()
    {
        cameraFollowTarget.rotation = Quaternion.Euler(xRotation, yRotation, 0f);

        if (characterController.velocity != Vector3.zero)
        {
            playerTransform.rotation = Quaternion.Euler(0f, yRotation, 0f);
        }

        mouseX = Input.GetAxis("Mouse X") * cameraSensitivity * Time.deltaTime;
        float mouseY = Input.GetAxis("Mouse Y") * cameraSensitivity * Time.deltaTime;

        xRotation -= mouseY;
        xRotation = Mathf.Clamp(xRotation, -15f, 20f);
        yRotation += mouseX;
    }
}
