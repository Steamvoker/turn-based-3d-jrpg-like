﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class BattleStats : MonoBehaviour
{
    #region Declarations
    [SerializeField] private TMP_Text nameText = null;
    [SerializeField] private Slider hpSlider = null;
    //[SerializeField] private TMP_Text humanityText = null;
    #endregion
    public virtual void HUDSetup(LivingEntity livingEntity)
    {
        nameText.text = livingEntity.LivingEntityName;
        hpSlider.maxValue = livingEntity.MaxHealth;
        hpSlider.value = livingEntity.CurrentHealth;
    }

    public void SetHP(int hp)
    {
        hpSlider.value = hp;
    }
}
