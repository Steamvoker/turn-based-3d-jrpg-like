﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseMenu : MonoBehaviour
{
    #region Declarations
    [SerializeField] private GameObject collectibleUI = null;
    [SerializeField] private GameObject pauseMenuUI = null;
    #endregion

    void Update()
    {
        if (collectibleUI.activeSelf == false && Input.GetKeyDown(KeyCode.Escape))
        {
            TogglePause();
        }
    }

    public void TogglePause()
    {
        Time.timeScale = 1 - Time.timeScale;
        Cursor.visible = !Cursor.visible;
        pauseMenuUI.SetActive(!pauseMenuUI.activeSelf);
    }

    public void MainMenu()
    {
        Time.timeScale = 1;
        SceneManager.LoadSceneAsync("MainMenu");
    }

    public void QuitGame()
    {
        Application.Quit();
    }
}
