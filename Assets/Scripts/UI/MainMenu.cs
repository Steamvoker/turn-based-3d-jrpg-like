﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
    public void PlayGame()
    {
        SceneManager.LoadScene("Logic");
        StartupSceneManager.LoadSceneAsyncAdditive("FreeRoamScene");
        StartupSceneManager.LoadSceneAsyncAdditive("UI");
        SceneManager.UnloadSceneAsync("MainMenu");
    }

    public void Quit()
    {
        Application.Quit();
    }
}
