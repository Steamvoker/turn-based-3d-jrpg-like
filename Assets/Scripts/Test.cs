﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Test : MonoBehaviour
{
    [SerializeField] private GameObject player = null;

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.T))
        {
            player.transform.position = new Vector3(9, -5.5f, 140);
        }
    }
}
