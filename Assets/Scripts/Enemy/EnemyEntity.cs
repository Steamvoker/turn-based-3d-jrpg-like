﻿using Cinemachine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyEntity : LivingEntity
{
    #region Declarations
    [SerializeField] private Transform enemyBattlePosition = null;
    [SerializeField] private Transform playerBattlePosition = null;
    [SerializeField] private CinemachineVirtualCamera battleSceneCamera = null;

    public Animator animator;
    public Animator PlayerAnimator;

    private Ability[] abilityArray = null;
    private EnemyEntity thisEnemy = null;

    public EnemyEntity ThisEnemy { get => thisEnemy; set => thisEnemy = value; }
    public Transform EnemyBattlePosition { get => enemyBattlePosition; set => enemyBattlePosition = value; }
    public Transform PlayerBattlePosition { get => playerBattlePosition; set => playerBattlePosition = value; }
    public CinemachineVirtualCamera BattleSceneCamera { get => battleSceneCamera; set => battleSceneCamera = value; }
    #endregion

    private void Awake()
    {
        thisEnemy = this;
        abilityArray = ThisEnemy.GetComponents<Ability>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other == other.GetComponent<CharacterController>())
        {
            LogicManager.Instance.CurrentEnemy = ThisEnemy;
            LogicManager.Instance.GameStateProperty = LogicManager.GameState.BATTLE;
        }
    }

    public void UseRandomAbility()
    {
        animator.SetTrigger("wispAttack");
        animator.SetTrigger("Attack#1");
        int ability = Random.Range(0, abilityArray.Length - 1);
        abilityArray[ability].PerformAbility();
        PlayerAnimator.SetTrigger("TakeDamage");
    }
}
