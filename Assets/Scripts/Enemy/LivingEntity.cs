﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum ElementalWeakness { NOTHING, WIND, FIRE, WATER, PHYSICAL, EARTH }

public class LivingEntity : MonoBehaviour
{
    #region Declarations
    [SerializeField] private string livingEntityName = null;
    [SerializeField] private int maxHealth = default;
    [SerializeField] private int weaknessDamageMultiplier = 2;
    [SerializeField] private ElementalWeakness elementalWeakness = default;

    private int currentHealth = default;

    public string LivingEntityName { get => livingEntityName; set => livingEntityName = value; }
    public int MaxHealth { get => maxHealth; set => maxHealth = value; }
    public int CurrentHealth { get => currentHealth; set => currentHealth = Mathf.Clamp(value, 0, MaxHealth); }
    #endregion Declarations

    private void Start()
    {
        CurrentHealth = MaxHealth;
    }

    public bool TakeDamage(int damage, ElementalWeakness damageType)
    {
        if (damageType.Equals(elementalWeakness))
        {
            damage *= weaknessDamageMultiplier;
        }

        CurrentHealth -= damage;

        return CurrentHealth <= 0;
    }
}
