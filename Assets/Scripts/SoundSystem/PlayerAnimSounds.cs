﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAnimSounds : MonoBehaviour
{
    GameObject audioManager;

    private void Step1()
    {
        FindObjectOfType<AudioManager>().Play("Step1");
    }

    private void Step2()
    {
        FindObjectOfType<AudioManager>().Play("Step2");
    }

    private void Jump()
    {
        FindObjectOfType<AudioManager>().Play("Jump");
    }

    private void JumpLanding()
    {
        FindObjectOfType<AudioManager>().Play("JumpLanding");
    }

    private void SnowStep1()
    {
        FindObjectOfType<AudioManager>().Play("SnowStep1");
    }

    private void SnowStep2()
    {
        FindObjectOfType<AudioManager>().Play("SnowStep2");
    }

    private void MagicAttack()
    {
        FindObjectOfType<AudioManager>().Play("MagicAttack");
    }

    private void BasicHit()
    {
        FindObjectOfType<AudioManager>().Play("BasicHit");
    }

    private void TakeDamage()
    {
        FindObjectOfType<AudioManager>().Play("TakeDamage");
    }   
}




