﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    #region Declarations
    [SerializeField] private GameObject collectibleTextUI = null;
    [SerializeField] private Slider playerHPBar = null;
    [SerializeField] private Slider enemyHPBar = null;
    [SerializeField] private PlayerStats playerStats = null;
    [SerializeField] private BattleStats enemyStats = null;
    [SerializeField] private TMP_Text combatNarrationText = null;
    [SerializeField] private TMP_Text humanityText = null;
    [SerializeField] private GameObject abilityButtonsParent = null;
    [SerializeField] private Button[] buttons = null;
    [SerializeField] private TMP_Text endingText = null;
    [SerializeField] private GameObject endingUI = null;

    public static UIManager Instance;
    public GameObject CollectibleTextUI { get => collectibleTextUI; set => collectibleTextUI = value; }
    public Slider PlayerHPBar { get => playerHPBar; set => playerHPBar = value; }
    public Slider EnemyHPBar { get => enemyHPBar; set => enemyHPBar = value; }
    public PlayerStats PlayerStats { get => playerStats; set => playerStats = value; }
    public BattleStats EnemyStats { get => enemyStats; set => enemyStats = value; }
    public TMP_Text CombatNarrationText { get => combatNarrationText; set => combatNarrationText = value; }
    public GameObject AbilityButtonsParent { get => abilityButtonsParent; set => abilityButtonsParent = value; }
    public Button[] Buttons { get => buttons; set => buttons = value; }
    public TMP_Text HumanityText { get => humanityText; set => humanityText = value; }
    public TMP_Text EndingText { get => endingText; set => endingText = value; }
    public GameObject EndingUI { get => endingUI; set => endingUI = value; }
    #endregion Declarations

    private void Awake()
    {
        if (!Instance)
            Instance = this;
        else
            Destroy(gameObject);
    }
}
