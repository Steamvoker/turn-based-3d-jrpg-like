﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class LogicManager : MonoBehaviour
{

    #region Declarations
    [SerializeField] private PlayerEntity playerEntityInstance = null;
    [SerializeField] private PlayerController playerController = null;
    [SerializeField] private EnemyEntity lastBoss = null;
    [SerializeField] private EndingSequence endingSequence = null;
    [SerializeField] private Animation_control_script playerAnimator = null;
    [SerializeField] private MouseLook mouseLook = null;

    private EnemyEntity currentEnemy = null;
    private GameState gameState = default;

    public event EventHandler OnFreeRoamUnload;
    public enum GameState { FREE_ROAM, BATTLE }
    public static LogicManager Instance;
    public PlayerEntity PlayerEntityInstance { get => playerEntityInstance; set => playerEntityInstance = value; }
    public PlayerController PlayerController { get => playerController; set => playerController = value; }
    public EnemyEntity CurrentEnemy { get => currentEnemy; set => currentEnemy = value; }
    public GameState GameStateProperty { get => gameState; set => gameState = value; }
    public MouseLook MouseLook { get => mouseLook; set => mouseLook = value; }
    public Animation_control_script PlayerAnimator { get => playerAnimator; set => playerAnimator = value; }
    #endregion Declarations

    private void Awake()
    {
        if (!Instance)
            Instance = this;
        else
            Destroy(gameObject);
        OnFreeRoamUnload += UnloadFreeRoamScene_LoadBattle;
    }

    private void Update()
    {
        if (GameStateProperty == GameState.BATTLE)
        {
            OnFreeRoamUnload?.Invoke(this, EventArgs.Empty);
        }

        if (BattleSceneManager.Instance)
        {
            if (BattleSceneManager.Instance.BattleState == BattleState.LOSS)
            {
                Application.Quit();
            }
        }

        if (!lastBoss.isActiveAndEnabled)
        {
            endingSequence.gameObject.SetActive(true);
        }
    }

    public void UnloadFreeRoamScene_LoadBattle(object sender, EventArgs eventArgs)
    {
        //SceneManager.UnloadSceneAsync("FreeRoamScene");
        StartupSceneManager.LoadSceneAsyncAdditive("BattleScene");
        OnFreeRoamUnload -= UnloadFreeRoamScene_LoadBattle;
        GameStateProperty = GameState.FREE_ROAM;
    }


}
