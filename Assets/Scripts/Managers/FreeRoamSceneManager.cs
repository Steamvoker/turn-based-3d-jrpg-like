﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FreeRoamSceneManager : MonoBehaviour
{
    #region Declarations
    public static FreeRoamSceneManager Instance;
    #endregion Declarations

    private void Awake()
    {
        if (!Instance)
            Instance = this;
        else
            Destroy(gameObject);

        Cursor.visible = false;
    }
}
