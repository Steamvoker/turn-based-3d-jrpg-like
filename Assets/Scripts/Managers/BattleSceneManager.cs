﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;
using System;
using UnityEngine.SceneManagement;

public class BattleSceneManager : MonoBehaviour
{
    #region Declarations
    [SerializeField] private BattleSystem battleSystem = null;


    public event EventHandler OnBattleSceneUnload;
    public static BattleSceneManager Instance;
    public BattleSystem BattleSystem { get => battleSystem; set => battleSystem = value; }
    public BattleState BattleState { get => BattleSystem.BattleState; set => BattleSystem.BattleState = value; }
    #endregion Declarations

    private void Awake()
    {
        if (!Instance)
            Instance = this;
        else
            Destroy(gameObject);

        OnBattleSceneUnload += UnloadBattleScene_LoadFreeRoam;


        FindObjectOfType<AudioManager>().Play("FightTheme");
        FindObjectOfType<AudioManager>().Stop("Ambient1");
    }

    private void Update()
    {
        //Debug.Log(battleSystem.Attacker);
    }

    public void UnloadBattleScene_LoadFreeRoam(object sender, EventArgs eventArgs)
    {
        SceneManager.UnloadSceneAsync("BattleScene");
        //StartupSceneManager.LoadSceneAsyncAdditive("FreeRoamScene");
        OnBattleSceneUnload -= UnloadBattleScene_LoadFreeRoam;
        LogicManager.Instance.OnFreeRoamUnload += LogicManager.Instance.UnloadFreeRoamScene_LoadBattle;

        FindObjectOfType<AudioManager>().Stop("FightTheme");
        FindObjectOfType<AudioManager>().Play("Ambient1");
    }

    public void InvokeSceneUnload()
    {
        OnBattleSceneUnload?.Invoke(this, EventArgs.Empty);
    }
}
