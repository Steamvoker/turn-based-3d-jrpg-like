﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class StartupSceneManager : MonoBehaviour
{
    #region Declarations
    public static StartupSceneManager Instance;
    #endregion Declarations

    private void Awake()
    {
        if (!Instance)
            Instance = this;
        else
            Destroy(gameObject);

        Cursor.lockState = CursorLockMode.Confined;

        LoadSceneAsyncAdditive("MainMenu");
        //LoadSceneAsyncAdditive("Logic");
        //LoadSceneAsyncAdditive("FreeRoamScene");
        ////LoadSceneAsyncAdditive("Level");
        //LoadSceneAsyncAdditive("UI");
    }

    private void Start()
    {
        SceneManager.UnloadSceneAsync("StartupScene");
    }

    public static void LoadSceneAsyncAdditive(string sceneName)
    {
        SceneManager.LoadSceneAsync(sceneName, LoadSceneMode.Additive);
    }
}
